import { useState } from 'react'
import './App.css'
import { Routes, Route } from 'react-router-dom';
import { Register } from './components/Register/Register.jsx';
import { Login } from './components/Login/Login';
import {Home} from './components/Home/Home'
import { CreateServices } from './components/CreateServices/CreateServices';

function App() {
  return (
    <div className="App">
      <header>
      <h1>Fixeer</h1>
      </header>
      <Routes>
        <Route path="/" element={<Home/>} />
        <Route path='/register' element={<Register/>}/>
        <Route path='/login' element={<Login/>}/>
        <Route path='/services' element={<CreateServices/>} />
      </Routes>
    </div>
  )
}

export default App
