import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useToken } from '../../TokenContext';

import './CreateServices.css';

export const CreateServices = () => {
    const [token] = useToken();
    const navigate = useNavigate();

    const [title, setTitle] = useState('')
    const [text, setText] = useState('');
    const [file, setFile] = useState();
    const [loading, setLoading] = useState(false);

    // Función que maneja el evento submit del formulario.
    const handleSubmit = async (e) => {
        e.preventDefault();

        setLoading(true);

        try {
            // Si queremos enviar un body con formato "form/data" es necesario crear un objeto
            // de tipo FormData y "pushear" los elementos que queramos enviar.
            const formData = new FormData();

            // Pusheamos las propiedades con append (no confundir este método con el append de DOM).
            formData.append('title', text)
            formData.append('text', text);
            formData.append('file', file);

            const res = await fetch('http://localhost:4000/services', {
                method: 'post',
                headers: {
                    Authorization: token,
                },
                body: formData,
            });

            const body = await res.json();

            if (body.status === 'error') {
                alert(body.message);
            } else {
                navigate('/');
            }
        } catch (err) {
            console.error(err);
        } finally {
            setLoading(false);
        }
    };

    return (
        <main className='tweet-create'>
            <form onSubmit={handleSubmit}>
                <h2>Crea un servicio</h2>
                <input type="text" placeholder='Title' onCHange={(e)=>setTitle(e.target.value)}></input>
                <textarea
                    placeholder='Description'
                    value={text}
                    onChange={(e) => setText(e.target.value)}
                    minLength='10'
                    required
                />
                <input
                    type='file'
                    onChange={(e) => setFile(e.target.files[0])}
                    required
                />

                <button disabled={loading}>Enviar</button>
            </form>
        </main>
    );
};

